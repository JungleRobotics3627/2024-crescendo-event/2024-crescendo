// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import com.pathplanner.lib.auto.AutoBuilder;
import com.pathplanner.lib.auto.NamedCommands;
import com.pathplanner.lib.commands.PathPlannerAuto;
import com.pathplanner.lib.path.GoalEndState;
import com.pathplanner.lib.path.PathConstraints;
import com.pathplanner.lib.path.PathPlannerPath;

import edu.wpi.first.math.MathUtil;
import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.math.controller.ProfiledPIDController;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.math.trajectory.Trajectory;
import edu.wpi.first.math.trajectory.TrajectoryConfig;
import edu.wpi.first.math.trajectory.TrajectoryGenerator;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.XboxController.Button;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.Constants.AutoConstants;
import frc.robot.Constants.DriveConstants;
import frc.robot.Constants.InputConstants.FlightStick;
import frc.robot.Constants.OIConstants;
import frc.robot.subsystems.ClimbSubsystem;
import frc.robot.subsystems.DriveSubsystem;
import frc.robot.subsystems.IntakeSubsystem;
import frc.robot.subsystems.ShooterSubsystem;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.RunCommand;
import edu.wpi.first.wpilibj2.command.SwerveControllerCommand;
import edu.wpi.first.wpilibj2.command.button.JoystickButton;
import edu.wpi.first.wpilibj2.command.button.CommandGenericHID;
import java.util.List;

/*
 * This class is where the bulk of the robot should be declared.  Since Command-based is a
 * "declarative" paradigm, very little robot logic should actually be handled in the {@link Robot}
 * periodic methods (other than the scheduler calls).  Instead, the structure of the robot
 * (including subsystems, commands, and button mappings) should be declared here.
 */
public class RobotContainer {
  // The robot's subsystems
  private final DriveSubsystem m_robotDrive = new DriveSubsystem();
  private final ShooterSubsystem m_shootSystem = new ShooterSubsystem();
  private final ClimbSubsystem m_climbSystem = new ClimbSubsystem();
  private final IntakeSubsystem m_IntakeSystem = new IntakeSubsystem();

  // The driver's controller
  XboxController m_driverController = new XboxController(OIConstants.kDriverControllerPort);
  Joystick m_FlightStick = new Joystick(FlightStick.FlightStickPort);
  // set up auto chooser
  private final SendableChooser<Command> autoChooser;

  /**
   * The container for the robot. Contains subsystems, OI devices, and commands.
   */
  public RobotContainer() {
    // Configure the button bindings
    configureButtonBindings();
    autoChooser = AutoBuilder.buildAutoChooser();
    SmartDashboard.putData("Auto Chooser", autoChooser);
    // Configure default commands
    m_robotDrive.setDefaultCommand(
        // The left stick controls translation of the robot.
        // Turning is controlled by the X axis of the right stick.
        new RunCommand(
            () -> m_robotDrive.drive(
                -MathUtil.applyDeadband(m_driverController.getLeftY(), OIConstants.kDriveDeadband),
                -MathUtil.applyDeadband(m_driverController.getLeftX(), OIConstants.kDriveDeadband),
                -MathUtil.applyDeadband(m_driverController.getRightX(), OIConstants.kDriveDeadband),
                true, true),
            m_robotDrive));
  }

  /**
   * Use this method to define your button->command mappings. Buttons can be
   * created by
   * instantiating a {@link edu.wpi.first.wpilibj.GenericHID} or one of its
   * subclasses ({@link
   * edu.wpi.first.wpilibj.Joystick} or {@link XboxController}), and then calling
   * passing it to a
   * {@link JoystickButton}.
   */
  // test configuration
  private void configureButtonBindings() {
    new JoystickButton(m_driverController, Button.kRightBumper.value)
        .whileTrue(new RunCommand(
            () -> m_robotDrive.setX(),
            m_robotDrive));

    
            // !! Boilerplate code for later on
    // Raise Climbers
    new JoystickButton(m_FlightStick, FlightStick.DeployClimbers)
        .whileTrue(new RunCommand(
            () -> m_climbSystem.spinMotorsForwards(),
            m_climbSystem)); 
    // Retract Climbers
    new JoystickButton(m_FlightStick, FlightStick.RetractClimbers)
        .whileTrue(new RunCommand(
            () -> m_climbSystem.spinMotorsBackwards(),
            m_climbSystem)); 
    // Spin the shooter forward
    new JoystickButton(m_FlightStick, FlightStick.SpinShooterForward)
        .whileTrue(new RunCommand(
            () -> m_shootSystem.spinMotorsForwards(),
            m_shootSystem)); 
    // Spin the shooter backward
    new JoystickButton(m_FlightStick, FlightStick.SpinShooterBackward)
        .whileTrue(new RunCommand(
            () -> m_shootSystem.spinMotorsBackwards(),
            m_shootSystem)); 
    // Spin the feeder forward
    new JoystickButton(m_FlightStick, FlightStick.SpinFeederForward)
        .whileTrue(new RunCommand(
            () -> m_IntakeSystem.spinFeederMotorForward(),
            m_IntakeSystem)); 
    // Spin the feeder backward
    new JoystickButton(m_FlightStick, FlightStick.SpinFeederBackward)
        .whileTrue(new RunCommand(
            () -> m_IntakeSystem.spinFeederMotorBackwards(),
            m_robotDrive));
    // 
    new JoystickButton(m_FlightStick, FlightStick.DeployIntake)
        .whileTrue(new RunCommand(
            () -> m_IntakeSystem.DeployIntake(),
            m_IntakeSystem));

    new JoystickButton(m_FlightStick, FlightStick.RetractIntake)
        .whileTrue(new RunCommand(
            () -> m_IntakeSystem.RetractIntake(),
            m_IntakeSystem));

    new JoystickButton(m_FlightStick, FlightStick.ExtendTrapMechanism)
        .whileTrue(new RunCommand(
            () -> m_climbSystem.extendTrap(),
            m_climbSystem));

    new JoystickButton(m_FlightStick, FlightStick.RetractTrapMechanism)
        .whileTrue(new RunCommand(
            () -> m_climbSystem.retractTrap(),
            m_climbSystem)); 
  }

  /**
   * Use this to pass the autonomous command to the main {@link Robot} class.
   *
   * @return the command to run in autonomous
   */
  public Command getAutonomousCommand() {
    return autoChooser.getSelected();
    }
}
