package frc.robot.subsystems;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkLowLevel.MotorType;

import edu.wpi.first.wpilibj2.command.SubsystemBase;

public class ClimbSubsystem extends SubsystemBase {
    // The CAN ids are temporary
    CANSparkMax leftclimber = new CANSparkMax(20, MotorType.kBrushed);
    CANSparkMax rightclimber = new CANSparkMax(21, MotorType.kBrushed);
    CANSparkMax extendtrapmotor = new CANSparkMax(22, MotorType.kBrushed);
    
    public void spinMotorsForwards(){
        setfollowers();
        rightclimber.set(0.2);
    }
    public void spinMotorsBackwards(){
        setfollowers();
        rightclimber.set(-0.2);
    }
    public void setfollowers(){
        leftclimber.follow(rightclimber,false);
    }
    public void extendTrap(){
        extendtrapmotor.set(0.2);
    }
    public void retractTrap(){
        extendtrapmotor.set(-0.2);
    }
}