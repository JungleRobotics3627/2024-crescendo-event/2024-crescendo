package frc.robot.subsystems;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkLowLevel.MotorType;

import edu.wpi.first.wpilibj2.command.SubsystemBase;

public class ShooterSubsystem extends SubsystemBase {
    // The CAN ids are temporary
    CANSparkMax left = new CANSparkMax(20, MotorType.kBrushless);
    CANSparkMax right = new CANSparkMax(21, MotorType.kBrushless);
    
    public void spinMotorsForwards(){
        setfollowers();
        right.set(0.2);
    }
    public void spinMotorsBackwards(){
        setfollowers();
        right.set(-0.2);
    }
    public void setfollowers(){
        left.follow(right,false);
    }
}
