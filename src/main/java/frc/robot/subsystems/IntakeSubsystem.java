package frc.robot.subsystems;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkLowLevel.MotorType;

import edu.wpi.first.wpilibj2.command.SubsystemBase;

public class IntakeSubsystem extends SubsystemBase {
    // The CAN ids are temporary
    CANSparkMax IntakeMotor = new CANSparkMax(20, MotorType.kBrushless);
    CANSparkMax FeederMotor = new CANSparkMax(21, MotorType.kBrushless);
    CANSparkMax IntakeRetractor = new CANSparkMax(22, MotorType.kBrushless);
    
    public void spinIntakeMotorForwards(){
        IntakeMotor.set(0.2);
    }
    public void spinIntakeMotorsBackwards(){
        IntakeMotor.set(-0.2);
    }
    public void spinFeederMotorForward(){
        FeederMotor.set(0.2);
    }
    public void spinFeederMotorBackwards(){
        FeederMotor.set(-0.2);
    }
    public void DeployIntake(){
        IntakeRetractor.set(0.2);
    }
    public void RetractIntake(){
        IntakeRetractor.set(-0.2);
    }
    
}