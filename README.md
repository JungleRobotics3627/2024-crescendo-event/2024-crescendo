# 2024 Crescendo
This is the reposity for robot code related to the 2024 Crescendo game. Formerly named Arrow.


## Timeline

- [x] [Finish Drivetrain Code](https://gitlab.com/JungleRobotics3627/2024-crescendo/-/settings/integrations)
- [x] [Finish Subsystem Implementation](https://gitlab.com/JungleRobotics3627/2024-crescendo/-/settings/integrations)
- [x] [Implement PathPlanner](https://gitlab.com/JungleRobotics3627/2024-crescendo/-/settings/integrations)
- [x] [Create Auto using PathPlanner](https://gitlab.com/JungleRobotics3627/2024-crescendo/-/settings/integrations)
- [ ] [Add Computer Vision systems to aid driving](https://gitlab.com/JungleRobotics3627/2024-crescendo/-/settings/integrations)


## Authors and acknowledgment
Mr. Sullivan - Programming Mentor<br>
Allen Lin - Programming Lead<br>
Andrew Ashby - Programming team member<br>
Andrew Kim - Programming team member<br>
Antonio Tata - Programming team member<br>
Divya Puthanveetil - Programming team member<br>
Lucas Yee - Programming team member<br>
Nathan Bowden - Programming team member<br>
Rohan Dash - Programming team member<br>
Seth Logan - Programming team member<br>

## License
This project is license by the MIT License.

## Project status
Currently Active as of the 2023-2024 Crescendo season.
